package by.vshkl.bashq.constants

class RatingIndicators {
    companion object {
        val ratingLeft = arrayOf(
                "(\uff65_\uff65 )", "(\u00ac_\u00ac )", "(\u0ca0_\u0ca0 )",
                "(\u0ca0\uff3f\u0ca0 )", "(\u0ca0\u76ca\u0ca0 )", "(>\uff3f< )", "(=\uff3f= )")
        val ratingRight = arrayOf(
                "( \uff65_\uff65)", "( \u00ac_\u00ac)", "( \u0ca0_\u0ca0)",
                "( \u0ca0\uff3f\u0ca0)", "( \u0ca0\u76ca\u0ca0)", "( >\uff3f<)", "( =\uff3f=)")
    }
}
