package by.vshkl.bashq.constants

class Urls {
    companion object {
        val urlNew = "http://bash.im"
        val urlRandom = "http://bash.im/random"
        val urlBest = "http://bash.im/best"
        val urlByRating = "http://bash.im/byrating"
        val urlAbyss = "http://bash.im/abyss"
        val urlAbyssTop = "http://bash.im/abysstop"
        val urlAbyssBest = "http://bash.im/abyssbest"
        val urlComicsCalendar = "http://bash.im/comics-calendar"
    }
}