package by.vshkl.bashq.view

import android.view.View

interface GalleryActionListener {

    fun onGalleryItemClicked(comicUrl: String, view: View)
}