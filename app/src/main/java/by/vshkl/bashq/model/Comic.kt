package by.vshkl.bashq.model

data class Comic(
        val thumbLink: String,
        val imageLink: String
)
