package by.vshkl.bashq.model

data class Rating(val rating: String,
                 val voteCount: Int = 0
)
