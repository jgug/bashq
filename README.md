# ** BashQ ** #

### Description ###

Mobile client for bash.im website writen in [Kotlin](https://kotlinlang.org/).

Key features:

* View quotes by sections
* Vote for quotes
* View comics

### Screenshots ###

![Quotes list](http://i.imgur.com/lUQnNo7m.png "Quotes list")
![Navigation drawer](http://i.imgur.com/raSHz1rm.png "Navigation drawer")
![Comics gallery](http://i.imgur.com/dIeXoa3m.png "Comics gallery")
![Comic view](http://i.imgur.com/IDOrsUjm.png "Comic view")